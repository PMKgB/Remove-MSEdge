# If not admin, rerun as admin
if (-Not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator')) {
	if ([int](Get-CimInstance -Class Win32_OperatingSystem | Select-Object -ExpandProperty BuildNumber) -ge 6000) {
		$CommandLine = "-File `"" + $MyInvocation.MyCommand.Path + "`" " + $MyInvocation.UnboundArguments
		Start-Process -FilePath PowerShell.exe -Verb Runas -ArgumentList $CommandLine
		Exit
	}
}
## Inlined Common stuff
# . "$PSScriptRoot\Common.ps1"

function Separator($text) {
	Write-Host ""
	Write-Host "----$($text -replace ".","-")----" -ForegroundColor Green
	Write-Host "|   $($text)   |" -ForegroundColor Green
	Write-Host "----$($text -replace ".","-")----" -ForegroundColor Green
	Write-Host ""
}

function Ask-YesNo {
	param (
		[Parameter(Mandatory = $true)]
		[string] $Question,
		[Parameter(Mandatory = $false)]
		[string] $Guide = "",
		[Parameter(Mandatory = $false)]
		[string] $ForegroundColor = "Cyan",
		[Parameter(Mandatory = $false)]
		[string] $BackgroundColor = "Black"
	)

	$yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes", "Returns true."
	$no = New-Object System.Management.Automation.Host.ChoiceDescription "&No", "Returns false."

	$options = [System.Management.Automation.Host.ChoiceDescription[]]($yes, $no)

	Write-Host $Question -ForegroundColor $ForegroundColor -BackgroundColor $BackgroundColor
	$result = $Host.UI.PromptForChoice("", $Guide, $options, 0)

	return $result -eq 0
}


Separator("Forcefully rip out Microsoft Edge")
Write-Host "EdgeWebView files will remain. Need to investigate implications." -ForegroundColor Yellow
if (!(Ask-YesNo "Exterminate?")) {
	exit
}


function Remove-RegistryAutorunItem {
	param(
		[string]$Pattern
	)

	$autorunLocations = @(
		"HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Run",
		"HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce",
		"HKCU:\Software\Microsoft\Windows\CurrentVersion\Run",
		"HKCU:\Software\Microsoft\Windows\CurrentVersion\RunOnce"
	)

	foreach ($location in $autorunLocations) {
		$items = Get-ItemProperty -Path $location -ErrorAction SilentlyContinue

		foreach ($item in $items.PSObject.Properties) {
			if ($item.Name -match $Pattern -or $item.Value -match $Pattern) {
				Write-Host "Removing autorun item '$($item.Name)' with value '$($item.Value)' from '$location'" -F Green
				Remove-ItemProperty -Path $location -Name $item.Name
			}
		}
	}
}


# Check running processes
$edgeProcesses = @(
	"msedge",
	"MicrosoftEdgeUpdate"
)
Write-Host "Terminate running Edge process" -ForegroundColor Cyan
foreach ($edgeProcess in $edgeProcesses) {
	$process = Get-Process -Name $edgeProcess -ErrorAction SilentlyContinue
	if ($process.Count -gt 0) {
		Write-Host "Stopping process: $process"
		Stop-Process $process -Force
	}
}

# Remove Autorun entries
Write-Host "Remove Edge registry autorun entries" -ForegroundColor Cyan
Remove-RegistryAutorunItem -Pattern "msedge"

# Remove some registry stuff
$edgeRegistryPaths = @(
	"HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{9459C573-B17A-45AE-9F64-1857B5D58CEE}",
	"HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Browser Helper Objects\{1FD49718-1D00-4B19-AF5F-070AF6D5D54C}",
	"HKLM:\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Explorer\Browser Helper Objects\{1FD49718-1D00-4B19-AF5F-070AF6D5D54C}",
	"HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\msedge.exe",
	"HKLM:\SOFTWARE\Microsoft\Edge"
)
Write-Host "Remove some Edge registry stuff" -ForegroundColor Cyan
foreach ($path in $edgeRegistryPaths) {
	if (Test-Path -Path $path) {
		Write-Host "Removing: $path" -F Green
		Remove-Item $path -Recurse -Force
	}
}


# Remove Program Files Folders
$EdgeFilePaths = @(
	"C:\Program Files (x86)\Microsoft\Edge",
	"C:\Program Files (x86)\Microsoft\EdgeCore",
	"C:\Program Files (x86)\Microsoft\EdgeUpdate",
	"C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Microsoft Edge.lnk"
	# 	"C:\Program Files (x86)\Microsoft\EdgeWebView"
)
Write-Host "Remove Edge from Program Files and ProgramData" -ForegroundColor Cyan
foreach ($filePath in $EdgeFilePaths) {
	if (Test-Path -Path $filePath) {
		Write-Host "Removing: $filePath" -F Green
		Remove-Item $filePath -Recurse -Force
	}
}

$edgeDataFolder = @(
	"$env:USERPROFILE\AppData\Local\Microsoft\Edge",
	"$env:USERPROFILE\AppData\Local\Microsoft\EdgeBho",
	"$env:USERPROFILE\AppData\LocalLow\Microsoft\EdgeBho"
)
Write-Host "Remove Edge from user AppData" -ForegroundColor Cyan
foreach ($folder in $edgeDataFolder) {
	if (Test-Path -Path $folder) {
		Write-Host "Removing: $folder" -F Green
		Remove-Item $folder -Recurse -Force
	}
}

# Check installed programs via registry (both 32-bit and 64-bit)
$programKeys = @(
	'HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall',
	'HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall'
)
Write-Host "Remove Edge in installed programs" -ForegroundColor Cyan
foreach ($key in $programKeys) {
	if (Test-Path -Path $key) {
		$installedPrograms = Get-ChildItem $key

		foreach ($program in $installedPrograms) {
			$programProperty = Get-ItemProperty $program.PsPath
			if ($programProperty.DisplayName -match "Microsoft Edge") {
				Write-Host "Removing: $($program.PsPath)" -ForegroundColor Green
				Remove-Item ($program.PsPath)
			}
		}
	}
}

# File type association
$classesKeys = @(
	"HKLM:\SOFTWARE\Classes\MSEdgeHTM",
	"HKLM:\SOFTWARE\Classes\MSEdgeMHT",
	"HKLM:\SOFTWARE\Classes\MSEdgePDF",
	"HKLM:\SOFTWARE\Clients\StartMenuInternet\Microsoft Edge"
)
Write-Host "Removing Edge file associations" -ForegroundColor Cyan
foreach ($key in $classesKeys) {
	if (Test-Path -Path $key) {
		Write-Host "Removing: $key" -F Green
		Remove-Item $key -Recurse
	}
}

function RemoveService {
	param(
		[string]$ServiceName
	)
	try {
		$service = Get-Service -Name $ServiceName -ErrorAction SilentlyContinue
		if ($null -eq $service) {
			return
		}
		Write-Host "Removing service: $ServiceName" -F Green
		Set-Service -Name $ServiceName -StartupType Disabled -ErrorAction Stop
		Stop-Service -Name $ServiceName -Force -ErrorAction Stop
		sc.exe delete $ServiceName
	} catch {
		Write-Host "RemoveService exception on: $ServiceName"
	}
}

# Known Services
$edgeServices = @(
	"BingSvc",
	"MicrosoftEdgeUpdate",
	"MicrosoftEdgeElevationService",
	"edgeupdate",
	"edgeupdatem"
)

Write-Host "Remove known Edge services" -ForegroundColor Cyan
foreach ($serviceName in $edgeServices) {
	RemoveService -ServiceName $serviceName
}

# TODO: Improve. Iterate over several varying patterns.
# Service matching
Write-Host "Remove Edge services by pattern" -ForegroundColor Cyan
$services = Get-Service -DisplayName "*Microsoft Edge*"
if ($null -ne $services) {
	foreach ($service in $services) {
		RemoveService -ServiceName $service.Name
	}
}

# Disable update / reinstalling?
Write-Host "Disable Edge update in registry" -ForegroundColor Cyan
$edgeUpdateKey = "HKLM:\SOFTWARE\Microsoft\EdgeUpdate"
$edgeUpdateDisableName = "DoNotUpdateToEdgeWithChromium"
if (-Not (Test-Path $edgeUpdateKey)) {
	New-Item -Path $edgeUpdateKey -Force
}

$updateValue = Get-ItemProperty -Path $edgeUpdateKey -Name $edgeUpdateDisableName -ErrorAction SilentlyContinue
if ($updateValue."$edgeUpdateDisableName" -ne 1) {
	New-ItemProperty -Path $edgeUpdateKey -PropertyType DWord -Name $edgeUpdateDisableName -Value 1 -Force
}


# TODO: Start menu Edge history crap

pause
